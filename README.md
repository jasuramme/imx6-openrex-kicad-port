# iMX6 Rex module Kicad port

iMX6 Rex module KiCad port.
Module official site is: https://www.imx6rex.com/

This is adapted for KiCad iMX6 Rex module.
It is not the exact copy of Altum board - some things were changed and adjusted by hands, so there possibly can be some mistakes.
If you found any issues or make any improvements, please contact me, fill bug report or create a merge request.

I hope you will find this project useful for your studies.
Please read the License, board is released under the Creative Commons Attribution 4.0 International license.

Unfortunately I used nightly build for porting openRex, so you have to use following KiCad version or newer one

(git) https://gitlab.com/kicad/code/kicad/-/commit/16b85ed56771d4b5af47010abedc4eab3b221c8a

(windows) https://kicad-downloads.s3.cern.ch/windows/nightly/kicad-r17275.16b85ed56-x86_64-lite.exe

# Screenshots

![layout](kicadLayout.png)

![3d](kicad3d.png)

![scheme](kicadscheme.png)
